﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5___Ivan_Kikic
{
    class NewNote : Note
    {
        public NewNote(string message, ITheme theme) : base(message, theme) { }

        private List<String> names = new List<String>();

        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("NOTE OF NAMES: ");
            string framedMessage = this.GetFramedMessage();

            foreach (String name in names)
            {
                Console.WriteLine(name);
            }

            Console.WriteLine(framedMessage); Console.ResetColor();
        }

        public void Add(String name)
        {
            names.Add(name);
        }

        public void Delete(String name)
        {
            names.Remove(name);
        }
    }
}
