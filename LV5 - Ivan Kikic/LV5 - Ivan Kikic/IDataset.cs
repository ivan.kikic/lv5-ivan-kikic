﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5___Ivan_Kikic
{
    interface IDataset
    {
        ReadOnlyCollection<List<string>> GetData();
    }
}
