﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5___Ivan_Kikic
{
    class ShippingService
    {
        private double WeightPrice;

        public ShippingService(double WeightPrice)
        {
            this.WeightPrice = WeightPrice;
        }

        public double Price(double weight)
        {
            return WeightPrice * weight;
        }
    }
}
