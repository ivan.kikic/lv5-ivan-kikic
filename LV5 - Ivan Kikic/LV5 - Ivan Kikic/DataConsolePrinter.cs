﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5___Ivan_Kikic
{
    class DataConsolePrinter
    {
        public void Print(IDataset dataset)
        {
            ReadOnlyCollection<List<string>> data = dataset.GetData();
            if(data == null)
            {
                Console.WriteLine("Pravo pristupa je onemogućeno!");
            } else
            {
                foreach(List<string> cols in data)
                {
                    foreach(string element in cols)
                    {
                        Console.Write(element + " ");
                    }
                    Console.WriteLine();
                }
            }
            Console.WriteLine();
        }
    }
}
