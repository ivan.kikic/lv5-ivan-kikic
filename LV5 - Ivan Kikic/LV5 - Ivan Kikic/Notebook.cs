﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5___Ivan_Kikic
{
    class Notebook
    {
        private List<Note> notes;
        private ITheme tema;

        public Notebook() { this.notes = new List<Note>(); }
        public Notebook(ITheme tema) { this.notes = new List<Note>(); this.tema = tema; }

        public void AddNote(Note note, ITheme tema)
        {
            this.notes.Add(note);
            foreach (Note nota in this.notes)
            {
                nota.Theme = tema;
            }
        }

        public void ChangeTheme(ITheme theme) { foreach (Note note in this.notes) { note.Theme = theme; } }

        public void Display() { foreach (Note note in this.notes) { note.Show(); Console.WriteLine("\n"); } }

        internal void AddNote(ReminderNote nota)
        {
            throw new NotImplementedException();
        }
    }
}
