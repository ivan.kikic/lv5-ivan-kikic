﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5___Ivan_Kikic
{
    class Program
    {
        static void Main(string[] args)
        {
            DataConsolePrinter printer = new DataConsolePrinter();

            User user1 = User.GenerateUser("Ime1");
            ProtectionProxyDataset prot1 = new ProtectionProxyDataset(user1);
            User user2 = User.GenerateUser("Ime2");
            ProtectionProxyDataset prot2 = new ProtectionProxyDataset(user2);
            User user3 = User.GenerateUser("Ime3");
            ProtectionProxyDataset prot3 = new ProtectionProxyDataset(user3);
            User user4 = User.GenerateUser("Ime4");
            ProtectionProxyDataset prot4 = new ProtectionProxyDataset(user4);
            User user5 = User.GenerateUser("Ime5");
            ProtectionProxyDataset prot5 = new ProtectionProxyDataset(user5);

            VirtualProxyDataset prot6 = new VirtualProxyDataset("ivankikic.csv");

            Console.WriteLine("#### Test ####\n");
            printer.Print(prot1);
            printer.Print(prot2);
            printer.Print(prot3);
            printer.Print(prot4);
            printer.Print(prot5);
            printer.Print(prot6);

            NewTheme tema1 = new NewTheme();
            LightTheme tema2 = new LightTheme();

            ReminderNote nota = new ReminderNote("Neki string message", tema1);

            nota.Show();

            NewNote NewNote = new NewNote("Sva imena", tema2);
            NewNote.Add("Ivan");
            NewNote.Add("Luka");
            NewNote.Add("Ante");
            NewNote.Show();
            NewNote.Delete("Ante");
            NewNote.Show();

            Console.WriteLine();
            Notebook notebook = new Notebook(tema1);
            notebook.AddNote(nota, tema2);
            notebook.Display();
            notebook.ChangeTheme(tema1);
            notebook.Display();
        }
    }
}
