﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5___Ivan_Kikic
{
    class NewTheme : ITheme
    {
        public void SetBackgroundColor()
        {
            Console.BackgroundColor = ConsoleColor.Red;
        }

        public void SetFontColor()
        {
            Console.ForegroundColor = ConsoleColor.DarkBlue;
        }

        public string GetHeader(int width)
        {
            return new string('#', width);
        }

        public string GetFooter(int width)
        {
            return new string('/', width);
        }
    }
}
